---
title: "La Soirée des Trophées Woman In Tech Sud 2019"
categories: fr chillcoding
author: macha
---

<div class="text-center lead" markdown="1">
  ![Macha Da Costa](/assets/img/post/macha-plage.jpg)
</div>
Photo : Olivier Ezratty pour http://www.qfdn.net [\[1\]](#qfdn)

Mon slogan « J'ai pas le temps de "Geeker", je suis à la plage ! » a pour objectif
d'interpeller les jeunes sur la mixité dans le milieu informatique.
Ce slogan peut être interprété de différentes façons.

<!--more-->

Jeudi 12 septembre 2019 a eu lieu la cérémonie des
<a href="https://www.youtube.com/watch?v=jaoPb1Wotyo" target="_blank">Trophées Women In Tech Sud</a> [\[2\]](#witsud),
organisée par WHAT06 [\[3\]](#what06), au Palais des festivals de Cannes.
J'ai eu la chance d'y être invitée et finaliste dans la catégorie entrepreneuse.

Pas besoin de le préciser, tout comme le bâtiment, les milieux techniques sont
"extrêmement féminin". En 2017, 16% de femmes occupent des métiers techniques,
d'après _Syntec Numérique_ [\[4\]](#nicematin). Il est urgent d'y remédier !

## Pourquoi il y a trop peu de femme ?
C'est un cliché : « un ingénieur en informatique a la réputation d'être un homme
pas très sexy » (boutonneux, pellicules et cheveux gras).
Heureusement, cet archétype masculin est une minorité (autrement je n'aurai pas
poursuivi mes études d'ingénieur en informatique).

« Vous ne pouvez pas vous permettre de ne pas jouer sur ce terrain de jeu ! » — Cristina Monnoyeur.

Une autre raison est l'auto-censure. En général, les filles pensent qu'elles ne réussiront
pas dans une filière technique. Dans ce système patriarcal, il
s'agit de s'affranchir de ces idées reçues. Une anecdote m'a beaucoup marquée durant
ma scolarité : en troisième mon professeur de mathématiques, dit tout à trac à ma mère
« C'est la première fois que je vois __une__ élève aussi douée en math. ».
Depuis, cela m'a toujours amusé de défier les préjugés :
jouer au foot, travailler à la plage [\[5\]](#geek), etc.

## Pourquoi il n'y a pas d'homme ?
Une autre anecdote est celle racontée par l'une des finalistes salariées,
lors de cette fabuleuse soirée. À l'issue d'une intervention
au lycée les Eucalyptus de Nice, dans le cadre de la journée
[les sciences de l'ingénieur au féminin](http://www.lessiaufeminin.fr/){:target="_blank"},
une fille lui a posé la question « Pourquoi les garçons ne sont pas conviés à ces rencontres ? ».
En effet, il ne s'agit pas de tomber dans l'excès inverse. Les hommes ont aussi
un rôle à jouer dans la féminisation du milieu technique. Nous avons besoin
du soutien des hommes affranchis des préjugés [\[6\]](#audrey), et cela dès le plus jeune âge.  
Idéalement, selon moi, il faudrait 48% d'hommes et 52% de femmes, dans tous les
milieux, cela afin de refléter notre société [\[7\]](#insee).

## Pourquoi il n'y a pas assez de diversité sociale ?
Dans la technique, il y a peu de personnes appartenant aux minorités. Il faut se
diriger vers des entreprises internationales, afin de voir plus de diversité.

Il est important d'avoir une mixité sociale dans tous les domaines. Cela dit,
les enjeux sont importants dans l'informatique.
Voici quelques exemples concrets, qui mettent en lumière les couacs de l'informatique :
* les lunettes de réalité virtuelle ne sont pas adaptées aux femmes
* la reconnaissance faciale ne fonctionne pas sur les femmes de couleurs [\[8\]](#faciale)
* les algorithmes d'Aladdin (_BlackRock_) ne prennent pas en compte
les enjeux environnementaux [\[9\]](#blackrock)

Dans le contexte de l'intelligence artificielle (IA), des décisions éthiques
sont prises par des humains, cela dans la programmation d'algorithmes susceptibles
de gérer nos vies. Il serait plus sage d'intégrer de la mixité sociale
afin d'avoir une richesse de points de vue.

Autrement, le manque de diversité conduit à une pauvreté intellectuelle.

Finalement, cette soirée m'a beaucoup inspirée et quelques personnalités m'ont interpellées
sur certains sujets :<br>
David Lisnard (l'ouverture de l'école [Simplon](https://simplon.co/cannes/){:target="_blank"} qui a tout intérêt à se diriger
vers des promotions mixtes), <br>
Françoise Bruneteaux (les milieux "extrêmement féminin"),<br>
Carole Malbrancq (l'auto-censure),<br>
Domitille Esnard-Domerego (les jouets sexistes),<br>
Marc-Paul Denamiel (un homme élevé par des femmes scientifiques),<br>
Imen Cherif (la niaque d'entreprendre),<br>
Karima Boudaoud (gagnante chercheuse),<br>
Gabrielle Regula (finaliste chercheuse),<br>
Marie Alejandra Zuluaga (finaliste chercheuse),<br>
Stéphanie Lavignasse (gagnante salariée),<br>
Christelle Yemdji Tchassi (finaliste salariée, anecdote « Pourquoi il n'y a pas d'homme ? »),<br>
Laurie Guliana (gagnante entrepreneuse),<br>
Pascale Caron (finaliste entrepreneuse),<br>
etc.

### {% icon fa-globe %} Références

1. <a name="qfdn"></a>[Quelques femmes du numérique, 2013](http://www.qfdn.net){:target="_blank"}
2. <a name="witsud"></a>[Trophées Woman In Tech Sud, 2019](https://www.what06.com/){:target="_blank"}
3. <a name="what06"></a>[Woman Hackers Action Tank, 2015](http://hackforwomen.com/){:target="_blank"}
4. <a name="nicematin"></a>[Nice-Matin : Pourquoi il est nécessaire d'avoir plus de femmes dans la tech, 13 septembre 2019](https://www.nicematin.com/economie/pourquoi-il-est-necessaire-davoir-plus-de-femmes-dans-la-tech-412684){:target="_blank"}
5. <a name="geek"></a>[Vidéo ChillCoding : Android - Introduction à Kotlin, 3 juillet 2017](https://www.youtube.com/watch?v=mrJmVjsJlYU){:target="_blank"}
6. <a name="audrey"></a>[Vidéo BDX I/O : The Hitchhiker's Guide to Diversity (Don't panic!), 9 novembre 2018](https://www.youtube.com/watch?time_continue=14&v=znX4pFJdiYg){:target="_blank"}
7. <a name="insee"></a>[Insee : Bilan démographique 2018, 15 janvier 2019](https://www.insee.fr/fr/statistiques/1892086?sommaire=1912926){:target="_blank"}
8. <a name="faciale"></a>[Le Figaro : La technologie de reconnaissance faciale est-elle raciste?, 2 juillet 2015](http://www.lefigaro.fr/secteur/high-tech/2015/07/02/32001-20150702ARTFIG00144-la-technologie-de-reconnaissance-faciale-est-elle-raciste.php){:target="_blank"}
9. <a name="blackrock"></a>[Vidéo ARTE : Ces financiers qui dirigent le monde (BlackRock), 17 septembre 2019](https://www.youtube.com/watch?v=Abz3Ab9HhaQ){:target="_blank"}
